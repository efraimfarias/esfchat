import play.GlobalSettings;
import play.Logger;
import securesocial.core.RuntimeEnvironment;
import services.MyEnvironment;
import java.util.HashMap;
import play.Application;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import scala.Tuple2;
import scala.collection.Seq;
import play.libs.F.Promise;
import static play.core.j.JavaResults.BadRequest;
import static play.core.j.JavaResults.InternalServerError;
import static play.core.j.JavaResults.NotFound;

import java.util.ArrayList;
import java.util.List;

import play.api.mvc.Results.Status;
import play.libs.Scala;

import play.api.mvc.EssentialFilter;
import play.filters.headers.SecurityHeadersFilter;



public class Global extends GlobalSettings {
	private RuntimeEnvironment env = new MyEnvironment();
	private HashMap<String, Object> instances = new HashMap<>();
	
	

	@Override
	public <A> A getControllerInstance(Class<A> controllerClass) throws Exception {
		A result = (A) instances.get(controllerClass.getName());
		Logger.debug("result = " + result);
		if ( result == null ) {
			Logger.debug("creating controller: " + controllerClass.getName());
			try {
				result = controllerClass.getDeclaredConstructor(RuntimeEnvironment.class).newInstance(env);
			} catch (NoSuchMethodException e) {
				// the controller does not receive a RuntimeEnvironment, delegate creation to base class.
				result = super.getControllerInstance(controllerClass);
			}
			instances.put(controllerClass.getName(), result);
		}
		return result;
	}
	
	 
}