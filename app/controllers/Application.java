package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import securesocial.controllers.routes;

import securesocial.core.java.SecureSocial;
import securesocial.core.java.SecuredAction;
import securesocial.core.java.UserAwareAction;

import services.MyUserService;
import services.User;

public class Application extends Controller {
	
	@UserAwareAction
	public static Result index() {
		User user = (User) ctx().args.get(SecureSocial.USER_KEY);
		
		if( user != null ) {
			return start();
		} else {
			return ok(views.html.index.render(user != null));
		}
			
	}
	
	@SecuredAction
	public static Result start() {
		User user = (User) ctx().args.get(SecureSocial.USER_KEY);

		String name = user.getMain().fullName().get();
		String avatarURL = user.getMain().avatarUrl().get();
		
		return ok(views.html.start.render(name,avatarURL));
	}
	
	
	
	/** It's called after login*/
	@SecuredAction
	public static Result begin() {
		User user = (User) ctx().args.get(SecureSocial.USER_KEY);
		String name = user.getMain().fullName().get();
				
		registerInOutRoom(name, true);
		
		return redirect(controllers.routes.Application.start());
	}
	
	/** It's called before logout */
	@SecuredAction
	public static Result end() {
		User user = (User) ctx().args.get(SecureSocial.USER_KEY);
		String name = user.getMain().fullName().get();
		
		//remove actual user
		MyUserService.deleteUser(user.getMain().providerId(), user.getMain().userId());
		
		registerInOutRoom(name, false);
		
		return redirect(routes.LoginPage.logout());
	}
	
	/** Register in/out the room for all */
	private static void registerInOutRoom(String name, Boolean login) {
		
		String msg = "[----- Saiu da Sala -----]";
		
		if(login) {
			msg = "[----- Entrou na Sala -----]";
		}
		
		String jsonNodeString = "{\"login\":\""+login+"\",\"user\":\""+ name +"\",\"text\":\""+msg+"\",\"room\":\"room1\"}";		
		ObjectMapper mapper = new ObjectMapper();
	    JsonNode jsNode;
		try {
			jsNode = mapper.readTree(jsonNodeString);
			
			//send to chat App
			ChatApplication.sendEvent(jsNode);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/** Using TreeMap and add into list of JSON **/
	//@SecuredAction
	public static Result users() {
		Map<String, String> usersLogged = new TreeMap<String, String>();
		
		//User inserting in order by Name. I am using treeMap. I created static method for users logged in MyService
		for ( User u: MyUserService.getUsers().values() ) {
			usersLogged.put(u.getMain().fullName().get(), u.getMain().avatarUrl().get());
		}
		
		
		List<String> listNodeUsers = new ArrayList<String>(); 
		
		for(Map.Entry<String,String> entry : usersLogged.entrySet()) {
			  String name = entry.getKey();
			  String avatarURL = entry.getValue();
			  
			  String jsonNodeString = "{\"user\":\""+ name +"\",\"avatarURL\":\""+avatarURL+"\"}";
			  
			  listNodeUsers.add(jsonNodeString);
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		JsonNode jsNode = mapper.convertValue(listNodeUsers, JsonNode.class);
		
		return ok(jsNode);
		
	}
	
	
}
