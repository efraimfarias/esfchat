package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.EventSource;
import play.mvc.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatApplication extends Controller {

  /** all connected per room **/
  private static Map<String, List<EventSource>> sockets = new HashMap<String, List<EventSource>>();

  
  /**
   * action for chat messages
   */
  //@SecuredAction
  public static Result postMsg() {
    sendEvent(request().body().asJson());
    return ok();
  }
    

  /**
   * Send event to all which are connected to the room
   */
  //@SecuredAction
  public static void sendEvent(JsonNode msg) {

    String room  = msg.findPath("room").textValue();
    if(sockets.containsKey(room)) {
      try {
		sockets.get(room).stream().forEach(es -> es.send(EventSource.Event.event(msg)));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }
  }

  /**
   * to keep track of which browser is in which room.
   */
  
  public static Result chatHeap(String room) {

    return ok(new EventSource() {
      @Override
      public void onConnected() {
        EventSource currentSocket = this;

        this.onDisconnected(() -> {

        	sockets.compute(room, (key, value) -> {
            if(value.contains(currentSocket))
              value.remove(currentSocket);
            return value;
          });
        });

        // Add socket to room
        sockets.compute(room, (key, value) -> {
          if(value == null)
            return new ArrayList<EventSource>() {{ add(currentSocket); }};
          else
            value.add(currentSocket); return value;
        });
      }
    });
  }
}