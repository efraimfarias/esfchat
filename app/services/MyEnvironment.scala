package services

import securesocial.core.RuntimeEnvironment
import securesocial.core.services.UserService
import securesocial.core.providers._

import scala.collection.immutable.ListMap

class MyEnvironment extends RuntimeEnvironment.Default[User] {
	override val userService: UserService[User] = new MyUserService()
	override lazy val providers = ListMap(
		
		include(new GoogleProvider(routes, cacheService, oauth2ClientFor(GoogleProvider.Google)))
	)
}
