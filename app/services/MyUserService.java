package services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.libs.F;
import play.libs.F.Promise;
import securesocial.core.BasicProfile;
import securesocial.core.PasswordInfo;
import securesocial.core.services.SaveMode;
import securesocial.core.java.BaseUserService;
import securesocial.core.java.Token;
import securesocial.core.providers.UsernamePasswordProvider;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MyUserService extends BaseUserService<User> {
	private static Logger logger = LoggerFactory.getLogger("userservice");
	private static HashMap<String, User> users = new HashMap<String, User>();
	
	/** Get all users logged */
	public static  HashMap<String, User> getUsers() {
		return users;
	}
	
	/** Delete user after log out */
	public static void deleteUser(String providerId, String userId) {
		logger.debug("**************************** DELETE USER " + users.get(providerId + userId));
		users.remove(providerId + userId);
		
	}
	
			
	@Override
	public F.Promise<User> doSave(BasicProfile profile, SaveMode mode) {
		User result = null;
		if (mode == SaveMode.SignUp()) {
			result = new User(profile);
			users.put(profile.providerId() + profile.userId(), result);
			logger.debug("**************************** SIGNUP");

		} else if (mode == SaveMode.LoggedIn()) {
			logger.debug("**************************** LOGIN");

			throw new RuntimeException("User logged");
		}
		return F.Promise.pure(result);
	}
	
	@Override
	public F.Promise<BasicProfile> doFind(String providerId, String userId) {

		
		BasicProfile found = null;
		for ( User u: users.values() ) {
			for ( BasicProfile i : u.identities ) {
				if ( i.providerId().equals(providerId) && i.userId().equals(userId) ) {
					found = i;
					break;
				}
			}
		}
		return F.Promise.pure(found);
	}

	@Override
	public void doDeleteExpiredTokens() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Promise<Token> doDeleteToken(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<BasicProfile> doFindByEmailAndProvider(String arg0, String arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<Token> doFindToken(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<User> doLink(User arg0, BasicProfile arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<PasswordInfo> doPasswordInfoFor(User arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<Token> doSaveToken(Token arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Promise<BasicProfile> doUpdatePasswordInfo(User arg0, PasswordInfo arg1) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
