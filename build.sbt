name := "esfChat"

version := "1.0-SNAPSHOT"

resolvers += Resolver.sonatypeRepo("snapshots")

lazy val root = (project in file(".")).enablePlugins(PlayJava).enablePlugins(SbtWeb)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
    javaCore, filters,
    "ws.securesocial" % "securesocial_2.11" % "3.0-M3",
    "org.webjars" % "bootstrap" % "3.2.0"
)

// libraryDependencies += filters

includeFilter in (Assets, LessKeys.less) := "*.less"

excludeFilter in (Assets, LessKeys.less) := "_*.less"
