/** Controllers for esf Chat */
angular.module('esfChat.controllers', ['esfChat.services']).
    controller('ChatCtrl', function ($scope, $http, chatModel) {
        $scope.rooms = chatModel.getRooms();
        $scope.msgs = [];
        $scope.users = [];
        $scope.inputText = "";
        $scope.currentRoom = $scope.rooms[0]; //Only a room
        
        
        /** Get the users logged */
        function addUsers() {
        	$http.get("/users").success(function(data) {
        		var usersLogged = JSON.parse("["+data+"]");
        	
        		$scope.users = usersLogged;
        	});
        };//End getUsers
        
        

              
        /** Post message in the chat  */
        $scope.submitMsg = function (name) {
            $http.post("/chat", { text: $scope.inputText, user: name,
                room: $scope.currentRoom.value });
            $scope.inputText = "";
        };//End submitMsg
        
        
        /** Add messages in array */
        $scope.addMsg = function (msg) {
            $scope.$apply(function () { 
            		var data = JSON.parse(msg.data);
            	          	
            		$scope.msgs.push(data);
            		
            		if( data.hasOwnProperty('login') ){
            			
            			addUsers();
            				
            			//var someArray = $scope.users.filter(person => person.user != data.user);
            			//$scope.users = someArray;
            				
            		} 
            		
            });//apply
            
            /** I dont know the position in the bottom chat **/
            var chatBox = document.getElementById('chatBox');
            chatBox.scrollTop = 300 + 8 + ($scope.msgs.length * 240);
        };//end addMsg;
        
        
        /** listening on messages from room */
        $scope.listen = function () {
            $scope.chatHeap = new EventSource("/chatHeap/" + $scope.currentRoom.value);
            $scope.chatHeap.addEventListener("message", $scope.addMsg, false);
            
        };//end listen
        
        //Add in users[]
        addUsers();
        
        $scope.listen();
        
    });