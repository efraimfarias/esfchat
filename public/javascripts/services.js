
angular.module('esfChat.services', []).service('chatModel', function () {
    var getRooms = function () {
        return [ { name: 'Room 1', value: 'room1'} ];
    };
    return { getRooms: getRooms };
});